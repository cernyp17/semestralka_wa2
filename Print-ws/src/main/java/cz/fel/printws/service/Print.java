/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cz.fel.printws.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * Print webservice based on CXF JAX webservice
 *
 * @author Pavel Cerny
 */

@WebService(targetNamespace = "http://printws.fel.cz/", name = "Print")
public interface Print {

	 @WebResult(name = "return", targetNamespace = "")
	    @RequestWrapper(localName = "printTicket", targetNamespace = "http://printws.fel.cz/", className = "cz.fel.printws.service.PrintTicket")
	    @WebMethod(action = "urn:printTicket")
	    @ResponseWrapper(localName = "printTicketResponse", targetNamespace = "http://printws.fel.cz/", className = "cz.fel.printws.service.PrintTicketResponse")
	    public java.lang.String printTicket(
	        @WebParam(name = "ticket", targetNamespace = "")
	        java.lang.String ticket
	    );
}