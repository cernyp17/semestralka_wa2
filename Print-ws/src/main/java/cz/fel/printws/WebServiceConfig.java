/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cz.fel.printws;

import org.springframework.boot.context.embedded.ServletRegistrationBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;

import cz.fel.printws.service.Print;
import cz.fel.printws.service.PrintImpl;

import org.apache.cxf.bus.spring.SpringBus;

import javax.xml.ws.Endpoint;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.cxf.jaxws.EndpointImpl;


/**
 * Print webservice based on CXF JAX webservice
 *
 * @author Pavel Cerny
 */


@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

	@Bean
	public ServletRegistrationBean dispatcherServlet() {
	    CXFServlet cxfServlet = new CXFServlet();
	    return new ServletRegistrationBean(cxfServlet, "/Service/*");
	}

	@Bean(name="cxf")
	public SpringBus springBus() {
	    return new SpringBus();
	}

	@Bean
	public Print myService() {
	    return new PrintImpl();
	}

	@Bean
	public Endpoint endpoint() {
	    EndpointImpl endpoint = new EndpointImpl(springBus(), myService());
	    endpoint.publish("/Print");
	    return endpoint;
	}
}