package cz.fel.print.service;

import cz.fel.aos.model.Reservation;

public interface PrintService {

  String printTicket(Reservation reservation);

}
