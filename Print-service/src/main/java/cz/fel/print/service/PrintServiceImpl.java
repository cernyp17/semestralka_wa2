package cz.fel.print.service;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cz.fel.aos.model.Reservation;
import cz.fel.aos.model.State;
import cz.fel.printws.client.Print;
import cz.fel.printws.client.PrintWsService;

@Service
public class PrintServiceImpl implements PrintService {

  private static final Logger log = LoggerFactory.getLogger(PrintServiceImpl.class);

  /**
   * Receive a message with reservation and generate e-ticket.
   * 
   * @param reservation
   */
  @Override
  public String printTicket(Reservation reservation) {
    log.debug("Print service triggered");

    // only accept paid reservations
    if (!reservation.getState().equals(State.PAID)) {
      log.debug("Reservation is not paid.");

      return "Reservation not paid!";
    }

    // format the ticket
    StringBuilder sb = new StringBuilder();
    sb.append("\n\n\n");
    sb.append("--------------------------\n");
    sb.append("FlightID: ");
    sb.append(reservation.getFlight().getName() + "\n");
    sb.append("Date: " + reservation.getDate() + "\n");
    sb.append("From: " + reservation.getFlight().getFromDestination().getName() + "\n");
    sb.append("To: " + reservation.getFlight().getToDestination().getName() + "\n\n");

    sb.append("Number of seats: " + reservation.getSeats() + "\n");
    sb.append("--------------------------\n\n\n");

    // send the e-ticket to print webservice
    log.debug("sending ticket to webservice");
    
    try {
		PrintWsService service = new PrintWsService(new URL("http://localhost:8080/Print-ws/Service/Print?wsdl"));
		Print client = service.getPrintWsPort();
		String response = client.printTicket(sb.toString());
		log.debug("ticket sucesfully sent");
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

    return sb.toString();

  }

}
