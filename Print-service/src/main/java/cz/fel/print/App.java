package cz.fel.print;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import cz.fel.aos.model.Reservation;
import cz.fel.print.service.PrintService;

@SpringBootApplication
@EnableJms
public class App extends SpringBootServletInitializer {

    @Autowired
    private PrintService print;

    @Autowired
    private ConnectionFactory connectionFactory;

    @Bean
    public DefaultMessageListenerContainer messageListener() {
        DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
        container.setConnectionFactory(this.connectionFactory);
        container.setDestinationName("testQueue");
        container.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                try {
                    Reservation reservation = (Reservation) message.getBody(Object.class);
                    print.printTicket(reservation);
                } catch (JMSException ex) {
                    ex.printStackTrace();
                }
            }
        });
        return container;
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
