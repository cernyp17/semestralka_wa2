package cz.fel.print.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import cz.fel.aos.model.Destination;
import cz.fel.aos.model.Flight;
import cz.fel.aos.model.Reservation;
import cz.fel.aos.model.State;

public class PrintServiceImplTest {

  @InjectMocks
  private PrintServiceImpl printService;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testService() {
    Reservation reservation = new Reservation();
    reservation.setDate("2016-07-16T19:20:30+01:00");
    reservation.setId(1);
    reservation.setPassword("pasw");
    reservation.setSeats(10);
    reservation.setState(State.PAID);

    Flight flight = new Flight();
    flight.setName("ABC-666");
    flight.setDateOfDeparture("2016-07-16T19:20:30+01:00");
    flight.setFromDestination(new Destination("Paris"));
    flight.setToDestination(new Destination("Berlin"));

    reservation.setFlight(flight);

    assertTrue(printService.printTicket(reservation).contains(flight.getName()));
  }

}
