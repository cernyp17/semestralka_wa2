package cz.fel.aos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Destination implements Serializable {

  private static final long serialVersionUID = 3L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private double lat;

  @Column(nullable = false)
  private double lon;

  // empty constructor for JPA
  protected Destination() {
  }

  public Destination(String name) {
    super();
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public double getLat() {
    return lat;
  }

  public double getLon() {
    return lon;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public void setLon(double lon) {
    this.lon = lon;
  }

  @Override
  public String toString() {
    return "Destination [id=" + id + ", name=" + name + ", lat=" + lat + ", lon=" + lon + "]";
  }

}
