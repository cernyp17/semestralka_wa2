package cz.fel.aos.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Flight implements Serializable {

  private static final long serialVersionUID = 2L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private String name;
  private String dateOfDeparture;
  private double distance;
  private double price;
  private int seats;

  @ManyToOne(fetch = FetchType.EAGER)
  private Destination fromDestination;

  @ManyToOne(fetch = FetchType.EAGER)
  private Destination toDestination;

  // empty constructor for JPA
  public Flight() {
  }

  public long getId() {
    return id;
  }

  public String getDateOfDeparture() {
    return dateOfDeparture;
  }

  public void setDateOfDeparture(String dateOfDeparture) {
    this.dateOfDeparture = dateOfDeparture;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getSeats() {
    return seats;
  }

  public void setSeats(int seats) {
    this.seats = seats;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Destination getFromDestination() {
    return fromDestination;
  }

  public void setFromDestination(Destination from) {
    this.fromDestination = from;
  }

  public Destination getToDestination() {
    return toDestination;
  }

  public void setToDestination(Destination to) {
    this.toDestination = to;
  }

}
