package cz.fel.aos.model;

public enum State {

  NEW, PAID, CANCELLED;

}
