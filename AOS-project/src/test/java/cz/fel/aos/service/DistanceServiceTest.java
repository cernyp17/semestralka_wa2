package cz.fel.aos.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import cz.fel.aos.model.Destination;

public class DistanceServiceTest {

  @InjectMocks
  private DistanceServiceImpl distanceService;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testService() {
    Destination dest1 = new Destination("Porto");
    Destination dest2 = new Destination("Madrid");

    Double distance = distanceService.computeDistance(dest1, dest2);

    assertTrue(distance > 0.0);
  }

}
