/*
 * Initialize destinations datatable.
 */
$(document).ready(function() {
	$('#destinations').DataTable({
		"processing" : true,
		"ajax" : {
			"url" : "/AOS-project/destination",
			"dataSrc" : ''
		},
		"columns" : [ {
			"data" : "id"
		}, {
			"mData" : "name",
			"mRender" : function(data, type, full) {
				return "<a href='dest.html?id=" + full.id
						+ "'>" + full.name + "</a>";
			}
		}, {
			"data" : "lat"
		}, {
			"data" : "lon"
		} ]
	});
});

/*
 * Initialize flights datatable.
 */
$.ajax({
	url : '/AOS-project/flight',
	type : 'GET',
	dataType : 'json',
	success : function(data) {
		assignToEventsColumns(data);
	}
});

function assignToEventsColumns(data) {
	var table = $('#flight').dataTable(
			{
				"bAutoWidth" : false,
				"aaData" : data,
				"columns" : [
						{
							"data" : "id"
						},
						{
							"data" : "name"
						},
						{
							"data" : "dateOfDeparture"
						},
						{
							"data" : "distance"
						},
						{
							"data" : "fromDestination",
							"render" : function(data, type, row, meta) {
								return "<a href='dest.html?id=" + data.id
										+ "'>" + data.name + "</a>";
							}
						},
						{
							"data" : "toDestination",
							"render" : function(data, type, row, meta) {
								return "<a href='dest.html?id=" + data.id
										+ "'>" + data.name + "</a>";
							}
						}, {
							"data" : "price"
						}, {
							"data" : "seats"
						} ]
			})
}