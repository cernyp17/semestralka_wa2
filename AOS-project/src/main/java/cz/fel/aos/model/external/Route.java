package cz.fel.aos.model.external;

public class Route {

  private String name;
  private double distance;

  public String getName() {
    return name;
  }

  public double getDistance() {
    return distance;
  }

}
