package cz.fel.aos.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import cz.fel.aos.model.Reservation;

@Service
public class Sender {

  private static final Logger log = LoggerFactory.getLogger(Sender.class);

  @Autowired
  private JmsTemplate jms;

  /**
   * Send reservation via JMS to the Print-Service to generate e-ticket.
   * 
   * @param reservation
   */
  public void sendTicket(Reservation reservation) {
    log.debug("---------------sending message----------------");
    jms.convertAndSend("testQueue", reservation);
  }

}
