package cz.fel.aos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

@Configuration
@EnableSwagger
@EnableAutoConfiguration
public class SwaggerConfig {
  private SpringSwaggerConfig springSwaggerConfig;

  /**
   * Autowired Spring support for Swagger (REST-api documentation tool).
   * 
   * @param springSwaggerConfig
   */
  @Autowired
  public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
    this.springSwaggerConfig = springSwaggerConfig;
  }

  /**
   * Bean providing custom Swagger configuration, e.q. REST endpoints, labels etc.
   * 
   * @return
   */
  @Bean
  public SwaggerSpringMvcPlugin customImplementation() {
    return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)
        // Root level documentation
        .apiInfo(new ApiInfo("Airline service REST API", "This service provides a representation the Airline service API", null, null, null, null))
        .useDefaultResponseMessages(false)
        // Map the specific URL patterns into Swagger
        .includePatterns(".*.*");
  }
}