package cz.fel.aos.service;

import cz.fel.aos.model.Destination;

public interface GeoService {

  Destination getDestinationWithCoordinates(Destination dest);

}
