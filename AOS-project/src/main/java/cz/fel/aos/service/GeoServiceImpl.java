package cz.fel.aos.service;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.LatLng;

import cz.fel.aos.model.Destination;

@Service
public class GeoServiceImpl implements GeoService {

  private static final Logger log = LoggerFactory.getLogger(GeoServiceImpl.class);

  /**
   * Send http request to Google GeoAPI and get the coordinates by destination name. The destination is updated with the retrieved coordinates.
   * 
   * @param Destination
   *          dest
   * 
   * @return updated Destination dest
   */
  @Override
  public Destination getDestinationWithCoordinates(Destination dest) {
    log.debug("Getting coordinates for destination: {}", dest.getName());

    final Geocoder geocoder = new Geocoder();
    GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(dest.getName()).setLanguage("en").getGeocoderRequest();
    try {
      GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
      List<GeocoderResult> results = geocoderResponse.getResults();
      LatLng latlng = results.get(0).getGeometry().getLocation();

      log.debug("Lat: {}, Lon: {}", latlng.getLat().doubleValue(), latlng.getLng().doubleValue());

      dest.setLat(latlng.getLat().doubleValue());
      dest.setLon(latlng.getLng().doubleValue());

    }
    catch (IOException e) {
      log.error("Error getting coordinates for destination.", e);
    }

    return dest;
  }

}
