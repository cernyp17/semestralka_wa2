package cz.fel.aos.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cz.fel.aos.model.Destination;
import cz.fel.aos.model.external.Route2Rio;

@Service
public class DistanceServiceImpl implements DistanceService {

  private static final Logger log = LoggerFactory.getLogger(DistanceServiceImpl.class);

  private String apiKey = "vwiC3pvW";

  /**
   * Sends a request to retrieve the routes between two destination to Rooute2Rio service.
   * 
   * @param Destination
   *          dest1, Destination dest2
   * @return double distance
   */
  @Override
  public double computeDistance(Destination dest1, Destination dest2) {
    log.debug("Getting distance from {} to {}", dest1.getName(), dest2.getName());

    RestTemplate restTemplate = new RestTemplate();

    String url = "http://free.rome2rio.com/api/1.2/json/Search?key=" + apiKey + "&oName=" + dest1.getName() + "&dName=" + dest2.getName();

    Route2Rio response = restTemplate.getForObject(url, Route2Rio.class);

    double distance = 0.0;
    if (response != null && response.getRoutes().length > 0) {
      distance = response.getRoutes()[0].getDistance();
    }

    log.debug("Result: {}, distance: {} ", response.getRoutes()[0].getName(), distance);

    return distance;
  }

}
