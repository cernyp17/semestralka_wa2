package cz.fel.aos.service;

import cz.fel.aos.model.Destination;

public interface DistanceService {

  double computeDistance(Destination dest1, Destination dest2);

}
