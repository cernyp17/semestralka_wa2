package cz.fel.aos.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cz.fel.aos.model.Flight;
import cz.fel.aos.model.Reservation;

/**
 * Spring CrudRepository. Exposes selected repository methods.
 * 
 * See Spring-boot documentation: http://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html
 * 
 * @author smajl
 */
public interface ReservationRepository extends CrudRepository<Reservation, Long> {

  List<Reservation> findAll();

  List<Reservation> findByFlight(Flight flight);

  Reservation findOne(Long id);

  void delete(Long id);

}
