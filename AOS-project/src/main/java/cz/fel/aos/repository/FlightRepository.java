package cz.fel.aos.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import cz.fel.aos.model.Flight;

/**
 * Spring CrudRepository. Exposes selected repository methods.
 * 
 * See Spring-boot documentation: http://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html
 * 
 * @author smajl
 */
public interface FlightRepository extends CrudRepository<Flight, Long> {

  List<Flight> findAll(Pageable pageable);

  Flight findOne(Long id);

  void delete(Long id);

}
