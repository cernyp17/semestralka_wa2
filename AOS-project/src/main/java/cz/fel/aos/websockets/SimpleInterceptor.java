package cz.fel.aos.websockets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

public class SimpleInterceptor extends ChannelInterceptorAdapter {

  private static final Logger log = LoggerFactory.getLogger(SimpleInterceptor.class);

  private static int connectedClients = 0;

  /**
   * Intercept client connect and disconnect events.
   * 
   * Increment and decrement number of connected clients on each event.
   */
  @Override
  public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

    StompHeaderAccessor sha = StompHeaderAccessor.wrap(message);

    // ignore non-STOMP messages like heartbeat messages
    if (sha.getCommand() == null) {
      return;
    }

    String sessionId = sha.getSessionId();

    switch (sha.getCommand()) {
    case CONNECT:
      connectedClients++;
      log.debug("STOMP Connect [sessionId: " + sessionId + "]");
      break;
    case CONNECTED:
      log.debug("STOMP Connected [sessionId: " + sessionId + "]");
      break;
    case DISCONNECT:
      connectedClients--;
      log.debug("STOMP Disconnect [sessionId: " + sessionId + "]");
      break;
    default:
      break;

    }
  }

  /**
   * Get number of connected clients.
   * 
   * @return int connectedClients
   */
  public static int getConnectClientsCount() {
    return connectedClients;
  }

}
