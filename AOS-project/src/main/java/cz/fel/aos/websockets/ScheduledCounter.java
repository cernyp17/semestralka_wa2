package cz.fel.aos.websockets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledCounter {

  @Autowired
  private SimpMessagingTemplate template;

  /**
   * Trigger socket messages to all connected clients on scheduled basis.
   */
  @Scheduled(fixedRate = 2000)
  public void trigger() {
    this.template.convertAndSend("/counter/count", "Clients: " + SimpleInterceptor.getConnectClientsCount());
  }

}
