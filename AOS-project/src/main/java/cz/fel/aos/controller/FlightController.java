package cz.fel.aos.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cz.fel.aos.model.Destination;
import cz.fel.aos.model.Flight;
import cz.fel.aos.repository.DestinationRepository;
import cz.fel.aos.repository.FlightRepository;
import cz.fel.aos.service.DistanceService;

@RestController
@RequestMapping("/flight")
public class FlightController {

  private static final Logger log = LoggerFactory.getLogger(FlightController.class);

  @Autowired
  private FlightRepository flightRepo;

  @Autowired
  private DestinationRepository destRepo;

  @Autowired
  private DistanceService distanceService;

  /**
   * Returns all flights in JSON/XML.
   * 
   * @return JSON/XML representation of the Flight list
   */
  @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = MediaType.ALL_VALUE)
  public List<Flight> getFlights(@RequestHeader(value = "X-Order", defaultValue = "name:asc") String xorder,
      @RequestHeader(value = "X-Base", defaultValue = "100") int xbase, @RequestHeader(value = "X-Offset", defaultValue = "0") int xoffset) {
    log.debug("Retrieving list of all flights.");

    // process ordering
    String[] ordering = xorder.split(":");

    // default is ascending
    Direction direction = Sort.Direction.ASC;
    if (ordering[1].equals("desc")) {
      direction = Sort.Direction.DESC;
    }

    log.debug("Order by {}, sort {}", ordering[0], direction);

    Sort sort = new Sort(direction, ordering[0]);
    PageRequest page = new PageRequest(xoffset, xbase, sort);

    return flightRepo.findAll(page);
  }

  /**
   * Returns flight based on id.
   * 
   * @return JSON/XML representation of the Flight object
   */
  @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = MediaType.ALL_VALUE)
  public Flight getFlight(@PathVariable long id) {
    log.debug("Retrieving a flight with id {}.", id);

    return flightRepo.findOne(id);
  }

  /**
   * Create new flight.
   * 
   * @return id of the flight
   */
  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public long createFlight(@RequestBody Flight flight, HttpServletResponse response) {
    log.debug("Creating a flight.");

    // check destination validity
    Destination from = flight.getFromDestination();
    Destination to = flight.getToDestination();

    // load destinations from repository
    from = destRepo.findOne(from.getId());
    to = destRepo.findOne(to.getId());
    if (from != null && to != null) {
      double distance = distanceService.computeDistance(from, to);

      flight.setDistance(distance);
      flight.setPrice(distance * 10);

      return flightRepo.save(flight).getId();
    }
    else {
      response.setStatus(HttpStatus.BAD_REQUEST.value());

      return 0;
    }

  }

  /**
   * Delete flight.
   */
  @RequestMapping(value = "{id}", method = RequestMethod.DELETE, consumes = MediaType.ALL_VALUE)
  public void deleteFlight(@PathVariable long id) {
    log.debug("Deleting a flight with id {}.", id);

    flightRepo.delete(id);
  }

  /**
   * Update existing flight.
   */
  @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
  public void updateFlight(@PathVariable long id, @RequestBody Flight flightNew, HttpServletResponse response) {
    log.debug("Updating a flight with id {}.", id);

    // update existing destination
    Flight flight = flightRepo.findOne(id);
    flight.setName(flightNew.getName());
    flight.setDateOfDeparture(flightNew.getDateOfDeparture());

    // check destination validity
    Destination from = flight.getFromDestination();
    Destination to = flight.getToDestination();
    if (destRepo.findOne(from.getId()) != null && destRepo.findOne(to.getId()) != null) {
      flight.setFromDestination(flightNew.getFromDestination());
      flight.setToDestination(flightNew.getToDestination());
    }
    else {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      return;
    }

    // compute distance
    flight.setDistance(flightNew.getDistance());

    // compute price
    flight.setPrice(flightNew.getPrice());

    flight.setSeats(flightNew.getSeats());

    // save changes
    flightRepo.save(flight);
  }
}
