package cz.fel.aos.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health")
public class HealthController {

  /**
   * Returns health of this application.
   * 
   * @return String 'UP and running'
   */
  @RequestMapping(method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE, consumes = MediaType.ALL_VALUE)
  public String getHealth() {
    return "UP and running";
  }

}
