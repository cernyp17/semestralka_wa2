package cz.fel.aos.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cz.fel.aos.jms.Sender;
import cz.fel.aos.model.Flight;
import cz.fel.aos.model.Payment;
import cz.fel.aos.model.Reservation;
import cz.fel.aos.model.State;
import cz.fel.aos.repository.FlightRepository;
import cz.fel.aos.repository.ReservationRepository;

@RestController
@RequestMapping("/reservation")
public class ReservationController {

  private static final Logger log = LoggerFactory.getLogger(ReservationController.class);

  // password length for a new reservation
  private static final int PWD_LENGTH = 5;

  @Autowired
  private ReservationRepository resRepo;

  @Autowired
  private FlightRepository flightRepo;

  @Autowired
  private Sender sender;

  /**
   * Returns all reservations in JSON/XML.
   * 
   * @return JSON/XML representation of the Reservation list
   */
  @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = MediaType.ALL_VALUE)
  public List<Reservation> getReservations() {
    log.debug("Retrieving list of all reservations.");

    return resRepo.findAll();
  }

  /**
   * Returns reservation based on id.
   * 
   * @return JSON/XML representation of the Reservation object
   */
  @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = MediaType.ALL_VALUE)
  public Reservation getReservation(@PathVariable long id, @RequestHeader(value = "X-Password") String xpassword, HttpServletResponse response) {
    log.debug("Retrieving a reservation with id {}.", id);

    Reservation reservation = resRepo.findOne(id);

    // check if user is authorized to retrieve this reservation
    if (reservation.getPassword().equals(xpassword)) {
      return resRepo.findOne(id);
    }
    else {
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      return null;
    }
  }

  /**
   * Create new reservation.
   * 
   * @return id of the reservation
   */
  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public long createReservation(@RequestBody Reservation reservation, HttpServletResponse response) {
    log.debug("Creating new reservation.");

    // set time-stamp
    TimeZone tz = TimeZone.getTimeZone("+01:00");
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    df.setTimeZone(tz);
    String nowAsISO = df.format(new Date());

    // password
    String password = RandomStringUtils.randomAlphanumeric(PWD_LENGTH);
    // TODO email password to client

    // verify seats
    // && save reservation
    if (isEnoughSeatsInPlane(reservation)) {
      reservation.setState(State.NEW);
      reservation.setDate(nowAsISO);
      reservation.setPassword(password);

      return resRepo.save(reservation).getId();
    }
    else {
      // no seats left, send error response
      response.setStatus(HttpStatus.CONFLICT.value());

      return 0;
    }
  }

  /**
   * Delete reservation.
   */
  @RequestMapping(value = "{id}", method = RequestMethod.DELETE, consumes = MediaType.ALL_VALUE)
  public void deleteReservation(@PathVariable long id) {
    log.debug("Deleteting reservation with id {}.", id);

    resRepo.delete(id);
  }

  /**
   * pay existing reservation.
   */
  @RequestMapping(value = "{id}/payment", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
  public void payReservation(@PathVariable long id, @RequestBody Payment payment, @RequestHeader(value = "X-Password") String xpassword,
      HttpServletResponse response) {
    log.debug("Changing state to PAID of reservation with id {}.", id);

    log.debug("Paying with credit card: {}", payment.getCardNumber());

    Reservation reservation = resRepo.findOne(id);

    // only authorized users can pay
    if (!reservation.getPassword().equals(xpassword)) {
      response.setStatus(HttpStatus.UNAUTHORIZED.value());

      return;
    }

    // only NEW can be paid
    if (reservation.getState() == State.NEW) {
      reservation.setState(State.PAID);
      resRepo.save(reservation);
    }
    else {
      response.setStatus(HttpStatus.CONFLICT.value());
    }

    sender.sendTicket(reservation);
  }

  /**
   * cancel existing reservation.
   */
  @RequestMapping(value = "{id}/cancel", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
  public void cancelReservation(@PathVariable long id, @RequestBody Reservation reservationNew, @RequestHeader(value = "X-Password") String xpassword,
      HttpServletResponse response) {
    log.debug("Changing state to CANCELLED of reservation with id {}.", id);

    Reservation reservation = resRepo.findOne(id);

    // only authorized users can pay
    if (!reservation.getPassword().equals(xpassword)) {
      response.setStatus(HttpStatus.UNAUTHORIZED.value());

      return;
    }

    // only NEW reservation can be cancelled
    if (reservation.getState() == State.NEW) {
      reservation.setState(State.CANCELLED);
      resRepo.save(reservation);
    }
    else {
      response.setStatus(HttpStatus.CONFLICT.value());
    }
  }

  /**
   * Verify if requested number of seats is less or equal to available number of seats
   * 
   * @param reservation
   * @return true if requested number is possible, false otherwise
   */
  private boolean isEnoughSeatsInPlane(Reservation reservation) {
    int noSeatsAvailable = noAvailableSeats(reservation.getFlight());
    int noSeatsRequested = reservation.getSeats();
    return noSeatsAvailable >= noSeatsRequested;
  }

  /**
   * Compute the number of available free places in Flight
   * 
   * @param flight
   * @return number of available places in Flight
   */
  private int noAvailableSeats(Flight flight) {
    // reload flight
    flight = flightRepo.findOne(flight.getId());

    int seatsTaken = 0;

    for (Reservation reservation : resRepo.findByFlight(flight)) {
      // do not count cancelled reservations
      if (reservation.getState() != State.CANCELLED) {
        seatsTaken += reservation.getSeats();
      }
    }

    int noSeatsAvailable = flight.getSeats() - seatsTaken;

    log.debug("Seats available: " + noSeatsAvailable);
    log.debug("Seats total: " + flight.getSeats());

    return noSeatsAvailable;
  }

}
