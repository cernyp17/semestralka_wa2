package cz.fel.aos.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cz.fel.aos.model.Destination;
import cz.fel.aos.repository.DestinationRepository;
import cz.fel.aos.service.GeoService;

@RestController
@RequestMapping("/destination")
public class DestinationController {

  private static final Logger log = LoggerFactory.getLogger(DestinationController.class);

  @Autowired
  private GeoService geoService;

  @Autowired
  private DestinationRepository destRepo;

  /**
   * Returns all destinations in JSON/XML.
   * 
   * @return JSON/XML representation of the Destination list
   */
  @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = MediaType.ALL_VALUE)
  public List<Destination> getDestinations() {
    log.debug("Retrieving list of all destinations.");

    return destRepo.findAll();
  }

  /**
   * Returns destination based on id.
   * 
   * @return JSON/XML representation of the Destination object
   */
  @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = MediaType.ALL_VALUE)
  public Destination getDestination(@PathVariable long id) {
    log.debug("Retrieving a destination with id {}.", id);

    return destRepo.findOne(id);
  }

  /**
   * Create new destination.
   * 
   * @return id of the destination
   */
  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public long createDestination(@RequestBody Destination destination) {
    log.debug("Creating new destination.");
    
    destination = geoService.getDestinationWithCoordinates(destination);

    return destRepo.save(destination).getId();
  }

  /**
   * Delete destination.
   */
  @RequestMapping(value = "{id}", method = RequestMethod.DELETE, consumes = MediaType.ALL_VALUE)
  public void deleteDestination(@PathVariable long id) {
    log.debug("Deleteting destination with id {}.", id);

    destRepo.delete(id);
  }

  /**
   * Update existing destination.
   */
  @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
  public void updateDestination(@PathVariable long id, @RequestBody Destination destinationNew) {
    log.debug("Updating destination with id {}.", id);

    // update existing destination
    Destination destination = destRepo.findOne(id);
    destination.setName(destinationNew.getName());
    destination.setLat(destinationNew.getLat());
    destination.setLon(destinationNew.getLon());

    // save changes
    destRepo.save(destination);
  }

}
