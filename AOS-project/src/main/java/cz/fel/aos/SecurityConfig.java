package cz.fel.aos;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  /**
   * Set admin user. Hard-coded for local development (add database users in live environment).
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication().withUser("admin").password("secret").roles("ADMIN");
  }

  /**
   * Security configuration for REST endpoints.
   */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    //@formatter:off
    http.csrf().disable().authorizeRequests()
      //login    
      .antMatchers(HttpMethod.GET, "/login.html").authenticated()
      // destination
      .antMatchers(HttpMethod.POST, "/destination/**").authenticated()
      .antMatchers(HttpMethod.PUT, "/destination/**").authenticated()
      .antMatchers(HttpMethod.DELETE, "/destination/**").authenticated()
      // flight
      .antMatchers(HttpMethod.POST, "/flight/**").authenticated()
      .antMatchers(HttpMethod.PUT, "/flight/**").authenticated()
      .antMatchers(HttpMethod.DELETE, "/flight/**").authenticated()
      //reservation
      .antMatchers(HttpMethod.GET, "/reservation").authenticated()
      .antMatchers(HttpMethod.DELETE, "/reservation/**").authenticated()
    .anyRequest().permitAll().and().httpBasic().and().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    //@formatter:on
  }
}
