package cz.fel.aos;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.core.config.Configuration;
import org.hornetq.core.remoting.impl.netty.NettyAcceptorFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.hornetq.HornetQConfigurationCustomizer;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@EnableJms
public class App extends SpringBootServletInitializer {

  public static void main(String[] args) {

    SpringApplication.run(App.class, args);
  }

  /**
   * Bean providing HornetQ setting, eq. host, port.
   * 
   * @return hornetCustomizer
   */
  @Bean
  public HornetQConfigurationCustomizer hornetCustomizer() {
    return new HornetQConfigurationCustomizer() {
      @Override
      public void customize(Configuration configuration) {
        Set<TransportConfiguration> acceptors = configuration.getAcceptorConfigurations();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("host", "localhost");
        params.put("port", "5445");
        TransportConfiguration tc = new TransportConfiguration(NettyAcceptorFactory.class.getName(), params);
        acceptors.add(tc);
      }
    };
  }

}
