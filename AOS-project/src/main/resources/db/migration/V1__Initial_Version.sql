CREATE TABLE destination (
    id BIGINT auto_increment,
    name VARCHAR(20) UNIQUE,
    lat DOUBLE null,
    lon DOUBLE null,
    PRIMARY KEY (`id`)
);

CREATE TABLE flight (
    id BIGINT auto_increment,
    name VARCHAR(30) UNIQUE,
    date_of_departure VARCHAR(30) null,
    seats INT null,
    price DOUBLE null,
    distance DOUBLE null,
    from_destination_id BIGINT,
    to_destination_id BIGINT,
    FOREIGN KEY (`from_destination_id`) REFERENCES `destination` (`id`),
    FOREIGN KEY (`to_destination_id`) REFERENCES `destination` (`id`),
    PRIMARY KEY (`id`)
);

CREATE TABLE reservation (
    id BIGINT auto_increment,
    state INT,
    password VARCHAR(30),
    email VARCHAR(40),
    seats INT null,
    date VARCHAR(30) null,
    flight_id BIGINT,
    CONSTRAINT `FK_FLIGHT` FOREIGN KEY (`flight_id`) REFERENCES `flight` (`id`),
    PRIMARY KEY (`id`)
);